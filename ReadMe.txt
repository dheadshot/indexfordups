DHeadshot's Software Creations IndexForDuplicates
Version: 0.01.00
Creates an index database to search for duplicate files.
Usage:
	indexfordups.exe [-?] [-O=ExistingDB] [-e=ExtensionList] [-d|-D] [-S] DirectoryPath
Where:
	-?	Writes this text
	-O=ExistingDB	Opens an existing index database (here called ExistingDB) and checks that
	-e=ExtensionList	Limits the indexing/checking to a list of file extensions of the form "a,b,c,d", e.g. -e=png,jpg,jpeg,gif,tif,tiff,bmp limits to common picture types.
	-d	Prompts to delete any duplicates found.
	-D	Automatically deletes any duplicates found!
	-S	Does not delete the index database when the program ends.
	DirectoryPath	The path of the directory to search.

-----------------------

Examples:
---------

To find duplicated pictures in the user's "Pictures" directory, run:
	indexfordups.exe -e=png,jpg,jpeg,gif,tif,tiff,bmp "%HOMEDRIVE%%HOMEPATH%\Pictures"
This will output a report on the commandline.

If you would rather output it into a file in Documents called "PicturesDuplicatesReport.txt", run:
	indexfordups.exe -e=png,jpg,jpeg,gif,tif,tiff,bmp "%HOMEDRIVE%%HOMEPATH%\Pictures" >"%HOMEDRIVE%%HOMEPATH%\Documents\PicturesDuplicatesReport.txt"

If, instead of a report, you want to delete the duplicates, run:
	indexfordups.exe -d -e=png,jpg,jpeg,gif,tif,tiff,bmp "%HOMEDRIVE%%HOMEPATH%\Pictures"
This will delete each duplicate after confirming with you first.

To delete them all automatically with NO WARNING, run:
	indexfordups.exe -D -e=png,jpg,jpeg,gif,tif,tiff,bmp "%HOMEDRIVE%%HOMEPATH%\Pictures"
(This is NOT recommended).


Please note that the above four examples require Windows Vista or newer - for Windows XP, the commands are:
	indexfordups.exe -e=png,jpg,jpeg,gif,tif,tiff,bmp "%HOMEDRIVE%%HOMEPATH%\My Pictures"
and
	indexfordups.exe -e=png,jpg,jpeg,gif,tif,tiff,bmp "%HOMEDRIVE%%HOMEPATH%\My Pictures" >"%HOMEDRIVE%%HOMEPATH%\PicturesDuplicatesReport.txt"
and
	indexfordups.exe -d -e=png,jpg,jpeg,gif,tif,tiff,bmp "%HOMEDRIVE%%HOMEPATH%\My Pictures"
and
	indexfordups.exe -D -e=png,jpg,jpeg,gif,tif,tiff,bmp "%HOMEDRIVE%%HOMEPATH%\My Pictures"



If you want to save the indexing database for a later query, include the "-S" flag.  You can then include it in a new query with the "-O=Database" flag.  This allows you to do stuff like this:
	indexfordups.exe -S "C:\Pictures"
  (This finds all duplicate files in "C:\Pictures" ...)
	indexfordups.exe -S -O=i4d.db -e=png,jpg,jpeg,gif,tif,tiff,bmp "C:\Pictures" >"C:\PicturesReport.txt"
  (... then writes a report of all the pictures in "C:\PicturesReport.txt" ...)
	indexfordups.exe -d -O=i4d.db -e=tif,tiff,bmp "C:\Pictures"
  (... then deletes the duplicate TIFFs and Bitmaps, seeing as they tend to be very large.)
Notice how the last command DOESN'T specify the "-S" flag.  This means that it will delete the database at the end!

Please note that if you build a database in the first place with a file extension list specified ("-e=ExtensionList" flag), ONLY those filetypes will exist in the database, so subsequent queries for other file types WILL NOT WORK!


----------------------

To compile:
-----------

A compiled executable is provided, however, if you would rather compile the code yourself:

With MinGW32, run:
	mingw32-make

OR

With MinGW64, run:
	mingw64-make

OR

With tcc, run:
	tcc -c createdb.c
	tcc -c filecompare.c
	tcc -c sqlite3.c
	tcc -c main.c
	tcc main.o createdb.o filecompare.o sqlite3.o -o indexfordups.exe



=======================
