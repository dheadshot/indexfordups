#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sqlite3.h"

#include "createdb.h"


int createdb(sqlite3 **db, sqlite3_stmt **insertstmt, sqlite3_stmt **updatedupstmt, const char *filename)
{
    sqlite3 *adb;
    int ret;
    char *errs;

    if ((ret = sqlite3_open(filename, db)) != SQLITE_OK)
    {
        fprintf(stderr, "Error creating database: %s!\n", sqlite3_errstr(ret));
        return 0;
    }
    adb = *db;

    ret = sqlite3_exec(adb, "CREATE TABLE IF NOT EXISTS \"Files\" ("
                            "Path TEXT PRIMARY KEY NOT NULL, "
                            "Extension TEXT NOT NULL DEFAULT '', "
                            "Size INTEGER, "
                            "Locked INTEGER NOT NULL DEFAULT 0, "
                            "Duplicated TEXT REFERENCES Files (Path) "
                            ");",
                            NULL, NULL, &errs);
    if (ret != SQLITE_OK && ret != SQLITE_DONE)
    {
        fprintf(stderr, "Error creating database: %s!\n", errs);
        sqlite3_free(errs);
        sqlite3_close(adb);
        return 0;
    }

    ret = sqlite3_prepare_v2(adb,
                             "INSERT INTO Files (Path, Extension, Size, Locked, Duplicated) "
                             "VALUES (?1, ?2, ?3, ?4, ?5);",
                             -1, insertstmt, NULL);
    if (ret != SQLITE_OK)
    {
        fprintf(stderr, "Error creating database: %s!\n", sqlite3_errstr(ret));
        sqlite3_finalize(*insertstmt);
        sqlite3_close(adb);
        return 0;
    }

    ret = sqlite3_prepare_v2(adb,
                             "UPDATE Files SET Duplicated = ?1 WHERE Path = ?2;",
                             -1, updatedupstmt, NULL);
    if (ret != SQLITE_OK)
    {
        fprintf(stderr, "Error creating database: %s!\n", sqlite3_errstr(ret));
        sqlite3_finalize(*insertstmt);
        sqlite3_finalize(*updatedupstmt);
        sqlite3_close(adb);
        return 0;
    }

    
    return 1;
}

int opendb(sqlite3 **db, sqlite3_stmt **insertstmt, sqlite3_stmt **updatedupstmt, const char *filename)
{
    sqlite3 *adb;
    int ret;
    char *errs;

    if ((ret = sqlite3_open(filename, db)) != SQLITE_OK)
    {
        fprintf(stderr, "Error opening database: %s!\n", sqlite3_errstr(ret));
        return 0;
    }
    adb = *db;

    ret = sqlite3_prepare_v2(adb,
                             "INSERT INTO Files (Path, Extension, Size, Locked, Duplicated) "
                             "VALUES (?1, ?2, ?3, ?4, ?5);",
                             -1, insertstmt, NULL);
    if (ret != SQLITE_OK)
    {
        fprintf(stderr, "Error opening database: %s!\n", sqlite3_errstr(ret));
        sqlite3_finalize(*insertstmt);
        sqlite3_close(adb);
        return 0;
    }

    ret = sqlite3_prepare_v2(adb,
                             "UPDATE Files SET Duplicated = ?1 WHERE Path = ?2;",
                             -1, updatedupstmt, NULL);
    if (ret != SQLITE_OK)
    {
        fprintf(stderr, "Error opening database: %s!\n", sqlite3_errstr(ret));
        sqlite3_finalize(*insertstmt);
        sqlite3_finalize(*updatedupstmt);
        sqlite3_close(adb);
        return 0;
    }

    
    return 1;
}

char *dbencodestring(const char *astr)
{
    int i,j=0, n;
    char *ans, *temp;
    n = strlen(astr)*2 + 1;
    temp = (char *) malloc(sizeof(char)*n);
    if (!temp) return NULL;
    for (i=0;astr[i]!=0;i++)
    {
        temp[j] = astr[i];
        j++;
        if (astr[i]=='\'')
        {
            temp[j] = '\'';
            j++;
        }
    }
    temp[j] = 0;
    n = strlen(temp)+1;
    ans = (char *) malloc(sizeof(char)*n);
    if (!ans)
    {
        free(temp);
        return NULL;
    }
    strcpy(ans,temp);
    free(temp);
    return ans;
}

