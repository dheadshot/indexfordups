#ifndef CREATEDB_H

#define CREATEDB_H 1

#include "sqlite3.h"

int createdb(sqlite3 **db, sqlite3_stmt **insertstmt, sqlite3_stmt **updatedupstmt, const char *filename);
int opendb(sqlite3 **db, sqlite3_stmt **insertstmt, sqlite3_stmt **updatedupstmt, const char *filename);
char *dbencodestring(const char *astr);

#endif
