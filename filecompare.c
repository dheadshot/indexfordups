#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "filecompare.h"


int IsFileSame(const char *FileName1, const char *FileName2)
{
    FILE *af, *bf;
    char abuf[FBUFSIZE], bbuf[FBUFSIZE];
    int ra, rb, ans=1;

    //    fprintf(stderr,"  -- Checking %s and %s\n",FileName1,FileName2);

    af=fopen(FileName1,"rb");
    if (!af)
    {
        fprintf(stderr, "Error opening file \"%s\"!\n",FileName1);
        return -1;
    }
    bf=fopen(FileName2,"rb");
    if (!bf)
    {
        fprintf(stderr, "Error opening file \"%s\"!\n",FileName2);
        fclose(af);
        return -2;
    }

    memset(abuf,0,FBUFSIZE);
    memset(bbuf,0,FBUFSIZE);

    do
    {
        ra = fread(abuf,1,FBUFSIZE,af);
        if (ra<FBUFSIZE && ferror(af))
        {
            fprintf(stderr, "Error reading from file \"%s\"!\n",FileName1);
            fclose(af);
            fclose(bf);
            return -1;
        }
    
        rb = fread(bbuf,1,FBUFSIZE,bf);
        if (rb<FBUFSIZE && ferror(bf))
        {
            fprintf(stderr, "Error reading from file \"%s\"!\n",FileName2);
            fclose(af);
            fclose(bf);
            return -2;
        }

        if (ra!=rb)
        {
            ans = 0;
            break;
        }
        if (ra && memcmp(abuf,bbuf,ra)!=0)
        {
            ans = 0;
            break;
        }
    } while (!feof(af) && !feof(bf));

    fclose(af);
    fclose(bf);
    return ans;
}
