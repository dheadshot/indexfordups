#Set these to tcc if compiling with TCC instead of MinGW!
CC=gcc
LD=gcc


indexfordups.exe: sqlite3.o createdb.o filecompare.o main.o
	${LD} main.o createdb.o filecompare.o sqlite3.o -o indexfordups.exe

createdb.o: createdb.c createdb.h sqlite3.h
	${CC} -c createdb.c

filecompare.o: filecompare.c filecompare.h
	${CC} -c filecompare.c

sqlite3.o: sqlite3.c sqlite3.h
	${CC} -c sqlite3.c

main.o: main.c createdb.h filecompare.h sqlite3.h
	${CC} -c main.c